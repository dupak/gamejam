﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scrolling : MonoBehaviour
{
    void Start()
    {
        
    }

    void Update()
    {
        transform.position = new Vector3 (transform.position.x, transform.position.y - Time.deltaTime, transform.position.z);
        if (transform.position.y < -20)
        {
            transform.position = new Vector3(transform.position.x, 30, transform.position.z);
        }
    }
}
