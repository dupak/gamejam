﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float FlyingSpeed = 20;

    // Sets up strings that will store the axis name depending on the player
    public string Horizontal_Axis, Vertical_Axis;

    public float xBounds, upperEdge, lowerEdge;

    private float hAxis, vAxis;

    private float xPos, yPos;

    private void Start()
    {
        if (gameObject.tag == "Player")
        {
            Horizontal_Axis = "Horizontal";
            Vertical_Axis = "Vertical";
        }
        else if (gameObject.tag == "Player2")
        {
            Horizontal_Axis = "Horizontal_P2";
            Vertical_Axis = "Vertical_P2";
        }
    }

    void GetInput()
    {
        hAxis = Input.GetAxis(Horizontal_Axis);
        vAxis = Input.GetAxis(Vertical_Axis);
    }

    void GetPos()
    {
        xPos = transform.position.x;
        yPos = transform.position.y;
    }

    void Update()
    {
        GetInput();
        GetPos();
    }

    void Movement()
    {
        //Vector2 newPos = new Vector2(hAxis, vAxis);
        //transform.Translate(newPos * Time.deltaTime * FlyingSpeed);
        transform.position = new Vector2(Mathf.Clamp(xPos, -xBounds, xBounds) + hAxis * Time.deltaTime * FlyingSpeed, Mathf.Clamp(yPos, lowerEdge, upperEdge) + vAxis * Time.deltaTime * FlyingSpeed);
    }
    private void FixedUpdate()
    {
        Movement();
    }
}
